(function(){
  'use strict';
  var Q = require('q');
  var rqst = require('request');
  
  var count = 0;
  var waiting = [];

  var request = function(options,callback){
    if(count < 20){
      count++;
      rqst(options,function(){
        count--;
        if(waiting.length){
          request.apply(null,waiting.shift());
        }
        callback.apply(this,arguments);
      });
    }else{
      waiting.push(arguments);
    }
  };

  var isFunction = function(obj){
      return !!(obj && obj.constructor && obj.call && obj.apply);
  };
  var makeAsyncQuery= function(query) {
    var deferred = Q.defer();
    var response = request({uri:query,method:'GET'},function(err,resp,body){
      if(!err && resp.statusCode == 200){
        deferred.resolve(JSON.parse(body));
      }else {
        deferred.reject(resp.statusCode);
      }
    });
    return deferred.promise;
  };
  var getLadder = function(league,start) {
      var query = 'http://api.pathofexile.com/ladders/'+league+'?offset='+start+'&limit=200';
      return makeAsyncQuery(query);
  };

  var getEvents = function() {
      return makeAsyncQuery('http://api.pathofexile.com/leagues?type=all');
  };
  module.exports.getEvents = getEvents;
  module.exports.getLadder = getLadder;
  return module;
}).call({},module);
